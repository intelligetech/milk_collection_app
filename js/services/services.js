angular.module('starter')

    .service('AuthService', function ($q, $http, USER_ROLES, BASE_URL, $ionicLoading) {
        var LOCAL_TOKEN_KEY = 'milkCollectionAppToken';
        var username = '';
        var isAuthenticated = false;
        var role = '';
        var authToken;
        var base_url = BASE_URL.base_url;
        var me = this;

        me.organisation = "";
        me.role_name = "";


        function loadUserCredentials() {
            var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
            if (token) {
                useCredentials(token);
            }
        }

        function storeUserCredentials(token) {
            window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
            //getAuthenticatedUser(token);
            useCredentials(token);
        }

        function getAuthenticatedUser(token) {
            me.organisation = "hello";

            $http({
                method: 'GET',
                url: base_url + "api/authenticate/user",
            }).success(function (data) {
                var user = data.user;
                window.localStorage.setItem('currentUser', JSON.stringify(user));

                //organisation = JSON.stringify(data.organisation);

                window.localStorage.setItem('currentOrganisation', JSON.stringify(data.organisation));
            }).error(function (err) {
                console.log("error getting auth user");
            });
        }

        function useCredentials(token) {
            username = token.split(',')[0];
            storedToken = token.split(',')[1];
            isAuthenticated = true;
            authToken = token;

            if (username == 'admin') {
                role = USER_ROLES.admin
            }
            if (username == 'user') {
                role = USER_ROLES.public
            }

            // Set the token as header for your requests!
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + storedToken;
        }

        function destroyUserCredentials() {
            authToken = undefined;
            username = '';
            isAuthenticated = false;
            $http.defaults.headers.common['Authorization'] = undefined;
            window.localStorage.removeItem(LOCAL_TOKEN_KEY);
            window.localStorage.clear();
        }

        var login = function (name, pw) {
            return $q(function (resolve, reject) {
                $http({
                    method: 'POST',
                    url: base_url + "api/authenticate",
                    data: {
                        "username": name,
                        "password": pw,
                    },
                }).success(function (data) {
                    var token = data.token;
                    $http.defaults.headers.common['Authorization'] = 'Bearer ' + data.token;
                    var user = data.user;
                    window.localStorage.setItem('currentUser', JSON.stringify(user));
                    var organisation = JSON.stringify(data.organisation);
                    me.organisation = data.organisation.org_name;
                    me.role_name = data.user.role_name;
                    window.localStorage.setItem('currentOrganisation', JSON.stringify(data.organisation));
                    window.localStorage.setItem('currentRole', JSON.stringify(data.user.role_name));
                    storeUserCredentials(name + ',' + token);
                    //getAuthenticatedUser(data.token);
                    resolve('Login success.');
                }).error(function (data) {
                    reject('Login Failed.');
                    $ionicLoading.hide();
                })
                /*                if ((name == 'admin' && pw == '1') || (name == 'user' && pw == '1')) {
                 // Make a request and receive your auth token from your server
                 storeUserCredentials(name + '.yourServerToken');
                 resolve('Login success.');
                 } else {
                 reject('Login Failed.');
                 }*/
            });
        };

        var logout = function () {
            destroyUserCredentials();
        };

        var isAuthorized = function (authorizedRoles) {
            if (!angular.isArray(authorizedRoles)) {
                authorizedRoles = [authorizedRoles];
            }
            return (isAuthenticated && authorizedRoles.indexOf(role) !== -1);
        };

        loadUserCredentials();


        return {
            login: login,
            logout: logout,
            isAuthorized: isAuthorized,
            isAuthenticated: function () {
                return isAuthenticated;
            },
            username: function () {
                return username;
            },
            organisation: function () {
                return me.organisation;
            },
            role: function () {
                return role;
            },
            role_name: function () {
                return me.role_name;
            }
        };
    })
    .factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
        return {
            responseError: function (response) {
                $rootScope.$broadcast({
                    401: AUTH_EVENTS.notAuthenticated,
                    403: AUTH_EVENTS.notAuthorized
                }[response.status], response);
                return $q.reject(response);
            }
        };
    })

    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    })

    //track farm activities id service

    .service('FarmService', function ($q, $http, USER_ROLES, BASE_URL, $ionicLoading) {
        return {
            selected_farm: function () {
                return JSON.parse(window.localStorage.getItem('selected_farm_details'));
            },
            farm: {
                id: '',
                farm_name: ''
            },
            update_farm: function (id, farm_name) {
                this.farm.id = id;
                this.farm.farm_name = farm_name;
            }
        };
    })
    .service('dataService', function ($q, $http, USER_ROLES, BASE_URL, $ionicLoading) {
        var base_url = BASE_URL.base_url;
        var farms = [];
        var getFarms = function (organisation_id) {
            /*get Farms list*/
            $http({
                method: 'POST',
                url: base_url + "api/getFarms",
                data: {
                    organisation_id: organisation_id
                }
            }).success(function (data) {
                if (data.message == 'success') {
                    farms = data.farms;
                    window.localStorage.setItem('farms', JSON.stringify(data.farms));
                } else {
                    console.log("error fetching farms");
                }
            }).error(function (data) {

            });
        }
        var getCollections = function (organisation_id) {
            /*get Farms list*/
            $http({
                method: 'POST',
                url: base_url + "api/getCollections",
                data: {
                    organisation_id: organisation_id
                }
            }).success(function (data) {
               // $ionicLoading.hide();
                if (data.message == 'success') {
                    farms = data.farms;
                    window.localStorage.setItem('acceptedCollections', JSON.stringify(data.acceptedCollections));
                    window.localStorage.setItem('rejectedCollections', JSON.stringify(data.rejectedCollections));

                } else {
                    console.log("error fetching farms");
                }
            }).error(function (data) {
                $ionicLoading.hide();
            });
        }
        /*get farmer to edit*/
        var saveFarmerEdit = function (farmerEdit) {
            /*get Farms list*/
            $http({
                method: 'POST',
                url: base_url + "api/saveFarmerEdit",
                data: {
                    farmerEdit: farmerEdit
                }
            }).success(function (data) {
                if (data.message == 'success') {
                    farms = data.farms;
                    window.localStorage.setItem('farms', JSON.stringify(data.farms));
                } else {
                    console.log("error fetching farms");
                }
            }).error(function (data) {

            });
        }
        return {
            getFarms: getFarms,
            farms: function () {
                //console.log(JSON.parse(window.localStorage.getItem('farms')));
                return JSON.parse(window.localStorage.getItem('farms'));
            },
            getCollections: getCollections,
            acceptedCollections: function () {
                //console.log(JSON.parse(window.localStorage.getItem('farms')));
                return JSON.parse(window.localStorage.getItem('acceptedCollections'));
            },
            rejectedCollections: function () {
                //console.log(JSON.parse(window.localStorage.getItem('farms')));
                return JSON.parse(window.localStorage.getItem('rejectedCollections'));
            },
            saveFarmerEdit:saveFarmerEdit
        }
    })