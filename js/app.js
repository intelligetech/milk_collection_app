// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'toastr', 'ion-floating-menu', 'ui.select', 'ngSanitize'])

/*.run(function($ionicPlatform) {
 $ionicPlatform.ready(function() {
 if(window.cordova && window.cordova.plugins.Keyboard) {
 // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
 // for form inputs)
 cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

 // Don't remove this line unless you know what you are doing. It stops the viewport
 // from snapping when text inputs are focused. Ionic handles this internally for
 // a much nicer keyboard experience.
 cordova.plugins.Keyboard.disableScroll(true);
 }
 if(window.StatusBar) {
 StatusBar.styleDefault();
 }
 });
 })*/
    .run(function ($rootScope, $state, AuthService, AUTH_EVENTS) {
        $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {

            if ('data' in next && 'authorizedRoles' in next.data) {
                var authorizedRoles = next.data.authorizedRoles;
                if (!AuthService.isAuthorized(authorizedRoles)) {
                    event.preventDefault();
                    $state.go($state.current, {}, {reload: true});
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                }
            }

            if (!AuthService.isAuthenticated()) {
                if (next.name !== 'login') {
                    event.preventDefault();
                    $state.go('login');
                }
            }
        });
    })
    .config(function ($stateProvider, $urlRouterProvider, USER_ROLES) {
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'templates/auth/login.html',
                reload: true,
                cache: false,
                controller: 'LoginCtrl'

            })
            .state('collector_dash', {
                url: 'collector/dash',
                templateUrl: 'templates/collector/collector_dashboard.html',
                reload: true,
                cache: false,
                controller: 'dashboardCtrl'
            })
            .state('newFarmer', {
                url: 'farmer/new',
                templateUrl: 'templates/farmer/newFarmer.html',
                reload: true,
                cache: false,
                controller: 'farmerCtrl'
            })
            .state('newFarm', {
                url: 'farm/new',
                templateUrl: 'templates/farm/farmerList.html',
                reload: true,
                cache: false,
                controller: 'farmerCtrl'
            })
            .state('listFarmers', {
                url: 'farmer/list',
                templateUrl: 'templates/farmer/farmerList.html',
                reload: true,
                cache: false,
                controller: 'farmerCtrl'
            })
            .state('listFarms', {
                url: 'farm/list',
                templateUrl: 'templates/farm/farmList.html',
                reload: true,
                cache: false,
                controller: 'farmerCtrl'
            })
            .state('profileFarmList', {
                url: 'profileFarm/list',
                templateUrl: 'templates/farm/profileFarmList.html',
                reload: true,
                cache: false,
                controller: 'farmerCtrl'
            })
            .state('farmActivities', {
                url: 'farm/Activities',
                templateUrl: 'templates/farm/farmActivities.html',
                reload: true,
                cache: false,
                controller: 'farmerCtrl'
            })
            .state('milkCollection', {
                url: 'milkCollection/dash',
                templateUrl: 'templates/collection/milkCollectionDash.html',
                reload: true,
                cache: false,
                controller: 'dashboardCtrl'
            })
            //milk collection states
            .state('addCollection', {
                url: 'collection/add',
                templateUrl: 'templates/collection/newCollection.html',
                reload: true,
                cache: false,
                controller: 'collectionCtrl'
            })

            .state('main', {
                url: '/',
                abstract: true,
                templateUrl: 'templates/collection/main.html',
                reload: true,
                cache: false,
                controller: 'collectionCtrl'
            })

            .state('main.accepted', {
                url: 'main/accepted',
                views: {
                    'accepted-tab': {
                        templateUrl: 'templates/collection/accepted.html',
                        reload: true,
                        cache: false,
                        controller: 'collectionCtrl'
                    }
                }
            })
            .state('main.rejected', {
                url: 'main/rejected',
                views: {
                    'rejected-tab': {
                        templateUrl: 'templates/collection/rejected.html',
                        reload: true,
                        cache: false,
                        controller: 'collectionCtrl'
                    }
                }
            })
            .state('main.admin', {
                url: 'main/admin',
                views: {
                    'admin-tab': {
                        templateUrl: 'templates/admin.html'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.admin]
                }
            })

            .state('farmer', {
                url: 'farmer',
                templateUrl: 'templates/Farmers/dash.html',
                reload: true,
                cache: false,
                controller: 'dashboardCtrl'
            })

            .state('Cash', {
                url: 'cash',
                templateUrl: 'templates/Farmers/cash.html',
                reload: true,
                cache: false,
                controller: 'Farmers'
            })

            .state('Farm', {
                url: 'farm',
                templateUrl: 'templates/Farmers/farm.html',
                reload: true,
                cache: false,
                controller: 'FarmersCtrl'
            })

            .state('Milk', {
                url: 'milk',
                templateUrl: 'templates/Farmers/milk.html',
                reload: true,
                cache: false,
                controller: 'Farmers'
            })

            //admin controlls
            .state('admin_dashboard', {
                url: 'admin_dashboard',
                templateUrl: 'templates/Admin/admin_dashboard.html',
                reload: true,
                cache: false,
                controller: 'dashboardCtrl'
            })

            .state('Reconciliations', {
                url: 'reconciliations',
                templateUrl: 'templates/Admin/reconciliations.html',
                reload: true,
                cache: false,
                controller: 'reconciliationsCtrl'
            })

            .state('notification',{
                url: 'notification',
                templateUrl: 'templates/Admin/notification.html',
                reload: true,
                cache: false,
                controller: 'notificationCtrl'
            })

            .state('thisDay',{
                url: 'thisDay',
                templateUrl: 'templates/Admin/thisDay.html',
                reload: true,
                cache: false,
                controller: 'notificationCtrl'
            })

            .state('thisWeek',{
                url: 'thisWeek',
                templateUrl: 'templates/Admin/thisWeek.html',
                reload: true,
                cache: false,
                controller: 'notificationCtrl'
            })

            .state('thisMonth',{
                url: 'thisMonth',
                templateUrl: 'templates/Admin/thisMonth.html',
                reload: true,
                cache: false,
                controller: 'notificationCtrl'
            })

        ;


        $urlRouterProvider.otherwise(function ($injector, $location) {
            var $state = $injector.get("$state");
            console.log(window.localStorage.getItem('currentRole'))
            if (window.localStorage.getItem('currentRole') == '"Admin"') {
                $state.go("admin_dashboard");
            } else {
                $state.go("collector_dash");
            }

        });
    })
    .config(function (toastrConfig) {
        angular.extend(toastrConfig, {
            autoDismiss: false,
            containerId: 'toast-container',
            maxOpened: 0,
            timeOut: 2000,
            newestOnTop: true,
            positionClass: 'toast-bottom-center',
            preventDuplicates: false,
            preventOpenDuplicates: false,
            target: 'body'
        });
    })
    .config(['$ionicConfigProvider', function ($ionicConfigProvider) {

        $ionicConfigProvider.tabs.position('top'); // other values: top

    }])

    .filter('propsFilter', function () {
        return function (items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function (item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    });
