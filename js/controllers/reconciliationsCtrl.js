/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

angular.module('starter')
    .controller('reconciliationsCtrl', function ($scope, toastr, $state, $ionicPopup, $http, BASE_URL, $ionicLoading, $ionicPlatform) {

        var base_url = BASE_URL.base_url;

        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var organisation_id = currentUser.organisation_id;
        var user_id = currentUser.id;

        //get all collectors
        $http({
            method: 'POST',
            url: base_url + "api/collectors",
            data: {
                organisation_id: organisation_id
            }
        }).success(function (data) {
            if (data.message == 'success') {
                $scope.all_collectors = data.all_collectors;
            } else {
                console.log("error fetching collectors");
            }
        }).error(function (data) {

        });

        /*get routes*/
        $http({
            method: 'POST',
            url: base_url + "api/getRoutes",
            data: {
                organisation_id: organisation_id
            }
        }).success(function (data) {
            if (data.message == 'success') {
                //$scope.routes = data.routes;

                $scope.data = {
                    availableOptions: data.routes,
                    selectedOption: {id: '0', name: 'Select a Route'} //This sets the default value of the select in the ui
                };
            } else {
                console.log("error fetching routes");
            }
        }).error(function (data) {

        });

        $scope.submitReconciliation = function (data) {
            console.log(data)
            $ionicLoading.show();
            if (data.category_id == undefined || data.selectedOption.id == 0) {
                $ionicLoading.show();
                toastr.error('Please provide all the collector details');
                $ionicLoading.hide();
                return;
            }
            if (data.selectedOption.id == 0) {
                $ionicLoading.show();
                toastr.error('Please provide  the collectors route');
                $ionicLoading.hide();
                return;
            }
            if (data.category_id == undefined) {
                $ionicLoading.show();
                toastr.error('Please provide the collectors name');
                $ionicLoading.hide();
                return;
            }

            $http({
                method: 'POST',
                url: base_url + "api/submitReconciliation",
                data: {
                    organisation_id: organisation_id,
                    collector_id: data.category_id,
                    route_id: data.selectedOption.id
                }
            }).success(function (data) {
                $ionicLoading.hide();
                if (data.message == 'success') {
                    $scope.reconciliation = data.collections;
                    if (data.collections.totalLitres == null || data.collections.totalLitres == '') {
                        $scope.reconciliation.totalLitres = 0;
                    } else {
                        $scope.reconciliation = data.collections;
                    }
                } else {
                    console.log("error fetching collectors reconciliation");
                }
            }).error(function (data) {
                $ionicLoading.hide();
            });

        }

        $scope.submitLitresBrought = function (data) {
            console.log(data)
            $ionicLoading.show();
            if (data.category_id == undefined || data.selectedOption.id == 0 || data.brought == undefined) {
                $ionicLoading.show();
                toastr.error('Please provide all the collector details');
                $ionicLoading.hide();
                return;
            }
            if (data.selectedOption.id == 0) {
                $ionicLoading.show();
                toastr.error('Please provide  the collectors route');
                $ionicLoading.hide();
                return;
            }
            if (data.category_id == undefined) {
                $ionicLoading.show();
                toastr.error('Please provide the collectors name');
                $ionicLoading.hide();
                return;
            }

            if (data.brought == undefined) {
                $ionicLoading.show();
                toastr.error('Please provide the collectors litres brought');
                $ionicLoading.hide();
                return;
            }

            $http({
                method: 'POST',
                url: base_url + "api/submitLitresBrought",
                data: {
                    organisation_id: organisation_id,
                    collector_id: data.category_id,
                    route_id: data.selectedOption.id,
                    litres_brought: data.brought
                }
            }).success(function (data) {
                $ionicLoading.hide();
                if (data.message == 'success') {
                    toastr.success('You have submitted the collectors milk');
                    $state.go('admin_dashboard');
                } else if (data.message == 'error1') {
                    toastr.error('The litres brought is greater than the recorded litres in the system');
                } else if (data.message == 'error2') {
                    toastr.error('The collector has not done any collections for this route today');
                }
                else {
                    toastr.error("You have already done reconciliations for the collector for this route today");
                }
            }).error(function (data) {
                $ionicLoading.hide();
            });
        }

    })