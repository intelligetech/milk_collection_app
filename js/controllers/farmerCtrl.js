angular.module('starter')
    .controller('farmerCtrl', function ($scope, $state, $ionicPopup, $ionicLoading, toastr, $ionicModal, $http, BASE_URL, FarmService, dataService) {
        var base_url = BASE_URL.base_url;

        $scope.loading = '';
        $ionicLoading.show();
        $scope.farm = {};
        /*get categories to display on register Farmer page*/
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser != undefined) {
            var organisation_id = currentUser.organisation_id;
            var user_id = currentUser.id;
            $scope.getData = function () {

            }
            $http({
                method: 'POST',
                url: base_url + "api/getCategories",
                data: {
                    organisation_id: organisation_id
                }
            }).success(function (data) {
                if (data.message == 'success') {
                    $scope.categories = data.categories;

                    $scope.cat = {
                        availableOptions: $scope.categories,
                        selectedOption: {id: '0', category_name: 'Select a Category'} //This sets the default value of the select in the ui
                    };
                } else {
                    console.log("error fetching categories");
                }
            }).error(function (data) {

            });
            /*get routes*/
            $http({
                method: 'POST',
                url: base_url + "api/getRoutes",
                data: {
                    organisation_id: organisation_id
                }
            }).success(function (data) {
                if (data.message == 'success') {
                    //$scope.routes = data.routes;

                    $scope.routes = {
                        availableOptions: data.routes,
                        selectedOption: {id: '0', name: 'Select a Route'} //This sets the default value of the select in the ui
                    };
                } else {
                    console.log("error fetching routes");
                }
            }).error(function (data) {

            });
            /*get Farmers list*/
            $http({
                method: 'POST',
                url: base_url + "api/getFarmers",
                data: {
                    organisation_id: organisation_id
                }
            }).success(function (data) {
                $ionicLoading.hide();

                if (data.message == 'success') {
                    $ionicLoading.hide();
                    $scope.loading = 'loading';
                    $scope.farmers = data.farmers;
                } else {
                    console.log("error fetching farmers");
                }
            }).error(function (data) {
                $ionicLoading.hide();

            });
            /*get Farms list*/
            $http({
                method: 'POST',
                url: base_url + "api/getFarms",
                data: {
                    organisation_id: organisation_id
                }
            }).success(function (data) {
                $ionicLoading.hide();
                if (data.message == 'success') {
                    $scope.farms = data.farms;
                } else {
                    console.log("error fetching farms");
                }
            }).error(function (data) {
                $ionicLoading.hide();
            });
            if ($scope.farmers == undefined) {
            }
            /*get activities and subactivities*/
            $http({
                method: 'POST',
                url: base_url + "api/getActvandSubActv",
                data: {
                    organisation_id: organisation_id
                }
            }).success(function (data) {
                if (data.message == 'success') {
                    $scope.activity_categs = data.actv_categs;
                    $scope.all_categs = data.all_categs.subCategs;
                    $scope.actv_categories = {
                        availableOptions: $scope.activity_categs,
                        selectedOption: {id: '0', category_name: 'Select a category'} //This sets the default value of the select in the ui
                    };
                } else {
                    console.log("error fetching counties");
                }
            }).error(function (data) {

            });
            //get breeds
            $http({
                method: 'POST',
                url: base_url + "api/getBreeds",
                data: {
                    organisation_id: organisation_id
                }
            }).success(function (data) {
                if (data.message == 'success') {
                    $scope.all_breeds = data.all_breeds;
                } else {
                    console.log("error fetching breeds");
                }
            }).error(function (data) {

            });
            //get farm activities
            $scope.selected_farm_id = FarmService.farm.id;

            $scope.getFarmActivitiesfunc = function () {
                $http({
                    method: 'POST',
                    url: base_url + "api/getFarmActivities",
                    data: {
                        organisation_id: organisation_id,
                        farm_id: $scope.selected_farm_id
                    }
                }).success(function (data) {
                    if (data.message == 'success') {
                        $scope.farmActivities_list = data.farmActivities;
                        if ($scope.farmActivities_list == "") {
                            $scope.noActivity = true;
                        } else {
                            $scope.noActivity = false;
                        }
                    } else {
                        $scope.noActivity = true;
                        console.log("error fetching activities");
                    }
                }).error(function (data) {

                });
            }
            $scope.getFarmActivitiesfunc();

        }
        $scope.newFarmer = function () {
            console.log("newFamer");
            $state.go('newFarmer');
        }
        //gender options
        $scope.gender = {
            availableOptions: [
                {id: '0', name: 'Select Gender'},
                {id: '1', name: 'Male'},
                {id: '2', name: 'Female'}

            ],
            selectedOption: {id: '0', name: 'Select a Gender'} //This sets the default value of the select in the ui
        }
        /*Add new farmer*/
        $scope.addFarmer = function (farmer, selected, gender) {
            $ionicLoading.show();
            if (farmer == undefined || farmer.first_name == undefined || farmer.surname == undefined || farmer.last_name == undefined ||
                farmer.memberNumber == undefined || farmer.id_number == undefined || farmer.phone == undefined || selected.id == 0 || gender.id == 0) {
                toastr.error('Please provide all the farmer details');
                $ionicLoading.hide();
                return;
            }
            farmer.organisation_id = organisation_id;
            farmer.category_id = selected.id;
            farmer.gender = gender.name;

            $http({
                method: 'POST',
                url: base_url + "api/addFarmer",
                data: {
                    "farmer": farmer,
                    "user_id": user_id
                }
            }).success(function (data) {
                $ionicLoading.hide();
                if (data.message == 'success') {
                    console.log("success");
                    $state.go('listFarmers');
                } else {
                    console.log("error adding farmer");
                }
            }).error(function (data) {
                $ionicLoading.hide();
                var errors = '';
                for (datos in data) {
                    errors += data[datos] + '</br>';
                    for (var i = 0; i < data[datos].length; i++) {
                        toastr.error(data[datos][i]);
                    }
                }
                console.log("http error");
                console.log(data);
            });
        };
        $scope.listFarmers = function () {

        }
        $scope.selectedFarmer = function (farmer) {
            $scope.farmer_id = farmer.id;
            $scope.farmer_name = '';
            $scope.openModal();
        }

        /*edit farmer details*/
        $scope.selectedFarmerEdit = function (farmer) {
            $scope.farmer_id = farmer.id;
            /*get farmer from server*/
            $scope.farmer = farmer;
            $scope.farmer.phone = parseInt(farmer.phone);
            $scope.farmer.id_number = parseInt(farmer.id_number);
            $scope.cat.selectedOption.id = parseInt(farmer.category_id);
            $scope.openEditFarmerModal();
        }
        /*save edited user*/
        $scope.saveEditedFarmer = function (farmer, selected) {
            $ionicLoading.show();
            if (farmer == undefined || farmer.first_name == undefined || farmer.surname == undefined || farmer.last_name == undefined ||
                farmer.id_number == undefined || farmer.phone == undefined || selected.id == 0) {
                toastr.error('Please provide all the farmer details');
                $ionicLoading.hide();
                return;
            }
            farmer.organisation_id = organisation_id;
            farmer.category_id = selected.id;
            $http({
                method: 'POST',
                url: base_url + "api/saveEditedFarmer",
                data: {
                    "farmer": farmer,
                    "farmer_id": $scope.farmer_id,
                    "user_id": user_id
                }
            }).success(function (data) {
                $ionicLoading.hide();
                if (data.message == 'success') {
                    console.log("success");
                    $scope.hideEditFarmerModal();
                    $state.go('listFarmers');
                } else {
                    console.log("error adding farmer");
                }
            }).error(function (data) {
                console.log("http error");
                $scope.hideEditFarmerModal();

            });
        }
        /*modal to edit farmer*/
        $ionicModal.fromTemplateUrl('templates/farmer/editFarmerModal.html', {
            scope: $scope,
            //animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.editFarmerModal = modal;
        });
        $scope.openEditFarmerModal = function () {
            $scope.editFarmerModal.show();
        };
        $scope.hideEditFarmerModal = function () {
            $scope.editFarmerModal.hide();
        }
        //modal to add new farm
        $ionicModal.fromTemplateUrl('templates/farm/addFarmModal.html', {
            scope: $scope,
            //animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.hideModal = function () {
            $scope.farm = {};
            console.log($scope.farm);
            $scope.modal.hide();
        }
        $scope.closeModal = function () {
            $scope.modal.remove();
        }
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            //$scope.modal.remove();
        });
        // Execute action on hide modal
        $scope.$on('modal.hidden', function (frequency) {
            // Execute action
        });
        // Execute action on remove modal
        $scope.$on('modal.removed', function () {
            // Execute action
        });
        /*get county data for the register farm data*/
        $http({
            method: 'GET',
            url: base_url + "api/getCounties",
            data: {}
        }).success(function (data) {
            if (data.message == 'success') {
                $scope.counties = data.county_names;
                $scope.all_county_data = data.all_county_data;
            } else {
                console.log("error fetching counties");
            }
        }).error(function (data) {

        });
        /*select on change events*/
        $scope.countyChange = function (cvalue) {
            console.log($scope.farm);
            if ($scope.farm) {
                if ($scope.farm.sub_county) {
                    $scope.farm.sub_county = undefined;
                }
                if ($scope.farm.ward) {
                    $scope.farm.ward = undefined;
                }
            }

            $scope.sub_counties = null;
            $scope.wards = null;
            if (cvalue == "") {
                toastr.error("Please select a county");
            } else {
                $scope.sub_counties = $scope.all_county_data[cvalue].sub_counties;
            }
            console.log($scope.farm);
        }
        $scope.sub_countyChange = function (cvalue, subcvalue) {
            console.log($scope.all_county_data);
            console.log(subcvalue);
            if ($scope.farm) {

                if ($scope.farm.ward) {
                    $scope.farm.ward = undefined;
                }
                console.log($scope.farm.county);
                $scope.wards = $scope.all_county_data[$scope.farm.county].county_data[subcvalue].wards;

            }

            console.log($scope.farm);
        }
        $scope.wardChange = function (cvalue) {
            if (cvalue == "") {
                toastr.error("Please select a ward");
            } else {
            }
        }
        /*get geolocation*/

        $scope.options = {maximumAge: 3000, timeout: 15000, enableHighAccuracy: true};

        $scope.successFunction = function (position) {
            $ionicLoading.hide();
            //add the geolocation to form data array
            $scope.farm.longitude = position.coords.longitude;
            $scope.farm.latitude = position.coords.latitude;
            toastr.success('successfully captured geo-location');

        }
        $scope.errorFunction = function (PositionError) {
            //customizing the message notification to the user..
            if (PositionError.code == 1) {//permission to use GEO api
                toastr.error('Turn on the geo-location');
            }
            else if (PositionError.code == 2) {//POSITION_UNAVAILABLE
                toastr.error('We could not determine your geo-location, Please try again.');
            }
            else if (PositionError.code == 3) {//TIMEOUT
                toastr.error('We couldn\'t determine your geo-location, retry.', 'Timeout exceeded!');
            } else {
                toastr.error(PositionError.message);
            }
            //the is error array here,,
            $ionicLoading.hide();
            //customize error messages
        }
        $scope.getLocation = function () {
            $ionicLoading.show();
            navigator.geolocation.getCurrentPosition($scope.successFunction, $scope.errorFunction, $scope.options);
        }
        /*Add new farm*/

        $scope.newFarm = function () {
            console.log("addFarm");
            $ionicLoading.show();
            $state.go('newFarm');
        }
        $scope.addFarm = function (farm, route) {
            $ionicLoading.show();
            if (farm == undefined || farm.farm_name == undefined || farm.county == undefined || farm.sub_county == undefined ||
                farm.ward == undefined || route.id == 0 || farm.latitude == undefined || farm.longitude == undefined) {
                toastr.error('Please provide new farm details');
                $ionicLoading.hide();
                return;
            }
            farm.farmer_id = $scope.farmer_id;
            farm.route_id = route.id;
            /*send new farm to server*/
            $http({
                method: 'POST',
                url: base_url + "api/addFarm",
                data: {
                    "farm": farm,
                    "user_id": user_id
                }
            }).success(function (data) {
                $ionicLoading.hide();
                if (data.message == 'success') {
                    console.log("success");
                    console.log(data);
                    $scope.hideModal();
                    $state.go('listFarms');
                } else {
                    console.log("error adding farm");
                }
            }).error(function (data) {
                $ionicLoading.hide();
                //$scope.hideModal();
                var errors = '';
                for (datos in data) {
                    errors += data[datos] + '</br>';
                    for (var i = 0; i < data[datos].length; i++) {
                        toastr.error(data[datos][i]);
                    }
                }
                console.log("http error");
            });
        }
        $scope.viewFarm = function (farm) {
            console.log("viewFarm");
            console.log(farm);
            $scope.farm = farm;
            $scope.openViewFarmModal(farm.latitude, farm.longitude);
            //open farm details modal
        }
        /*view farm details modal*/
        //modal to add new farm activity
        $scope.createViewFarmModal = function () {
            $ionicModal.fromTemplateUrl('templates/farm/viewFarmModal.html', {
                scope: $scope,
                //animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.viewFarmModal = modal;
            });
        }
        $scope.createViewFarmModal();
        $scope.openViewFarmModal = function (lat, long) {
            console.log(lat + long);
            $scope.viewFarmModal.show();

            var latLng = new google.maps.LatLng(lat, long);
            var mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
            google.maps.event.addListenerOnce($scope.map, 'idle', function () {

                var marker = new google.maps.Marker({
                    map: $scope.map,
                    animation: google.maps.Animation.DROP,
                    position: latLng
                });

                var infoWindow = new google.maps.InfoWindow({
                    content: "Here I am!"
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infoWindow.open($scope.map, marker);
                });
            });
        };
        $scope.hideViewFarmModal = function () {
            $scope.viewFarmModal.hide();
            $scope.map = null;
            $state.reload();
        }
        $scope.closeViewFarmModal = function () {
            $scope.viewFarmModal.remove();
            $scope.map = null;
            $state.reload();
        }
        /*profile a Farm*/
        $scope.selectedFarm = function (farm) {
            $ionicLoading.show();
            window.localStorage.setItem('selected_farm_details', JSON.stringify(farm));
            FarmService.update_farm(farm.id, farm.farm_name);
            $state.go('farmActivities', {reload: true});
            //$scope.getFarmActivities();

            //$scope.openModal();
        }
        $scope.selected_farm_name = FarmService.farm.farm_name;
        $scope.selected_farm_id = FarmService.farm.id;
        //$scope.noActivity = true;

        //get farm activities
        $scope.getFarmActivities = function () {
        }

        /*Add new farm activity*/
        $scope.newActivity = function (farmActivity) {
            $scope.openFarmActivityModal();
        }
        //modal to add new farm activity
        $scope.createActModal = function () {
            $ionicModal.fromTemplateUrl('templates/farm/addFarmActivityModal.html', {
                scope: $scope,
                //animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.farmActivityModal = modal;
            });
        }
        $scope.createActModal();
        $scope.openFarmActivityModal = function () {
            $scope.farmActivityModal.show();
        };
        $scope.hideFarmActivityModal = function () {
            $scope.farmActivityModal.hide();
            $state.reload();

        }
        $scope.closeFarmActivityModal = function () {
            $scope.farmActivityModal.remove();
            $state.reload();
        }
        /*get activities and sub_activities*/
        $scope.getActivitiesAndSubActivities = function () {

        }
        $scope.activitySelectChange = function (selected_actv) {
            $scope.sub_actv_categories = null;
            if (selected_actv.id == 0) {
                toastr.error("Please select a category");
            } else {
                $scope.sub_actv_categories = {
                    availableOptions: $scope.all_categs[selected_actv.name],
                    selectedOption: {id: '0', category_name: 'Select a sub category'} //This sets the default value of the select in the ui
                };
            }
            if (selected_actv.name == 'Animal Husbandry') {
                $scope.show_animals = true;
            } else {
                $scope.show_animals = false;
            }
        }
        $scope.subactivitySelectChange = function (selected_sub_actv) {
            console.log(selected_sub_actv);
            if (selected_sub_actv == undefined) {
                toastr.error("Please select a sub category");
            } else {
                console.log($scope.all_breeds[selected_sub_actv.name]);
                /*breeds list*/
                $scope.breed_list = {
                    availableOptions: $scope.all_breeds[selected_sub_actv.name],
                    selectedOption: {
                        id: '0', name: 'Select a Breed'
                    } //This sets the default value of the select in the ui
                }
                ;
            }
        }
        /*farm activity details*/
        /*add breed clone div*/
        $scope.breed_no = "";
        $scope.clicks = 0;
        /*add first panel*/
        $scope.panelnumber = 0;
        $scope.panels = [];
        $scope.remaining_animals = [];
        $scope.addPanels = function (count) {
            console.log("add panels");
            $scope.panels = [];
            $scope.remaining_animals = [];
            $scope.activity_details = [];
            $scope.panelnumber = 0;
            if ((count == "") || (count == 0) || (count == undefined)) {
                $scope.panels = [];
                toastr.error("number cannot be 0 or empty");
                $scope.breed_no = "";
                $scope.all_animals = 0;
                $scope.panelnumber = 0;
                $scope.remaining_animals = [];
                $scope.activity_details = [];
            } else {
                $scope.all_animals = count;
                $scope.remaining_animals[1] = count;
                $scope.panelnumber += 1;
                $scope.panels.push($scope.panelnumber);
                $scope.added = [];
            }
            console.log($scope.panelnumber);
        }
        /*add extra panels based on initial panel this depends on the number of animals remaining
         * if initial was 3, 3 will be available,
         * if next panel
         * */
        $scope.additionalPanels = function (count, n, activity_dets) {
            console.log("additional panel" + n);
            console.log("panel number" + $scope.panelnumber);
            console.log("scope [n]" + $scope.added[n]);
            var localpanelcount = $scope.panelnumber;
            if ($scope.added[n] == 1) {
                if ($scope.panelnumber > n) {
                    /*delete child panels*/
                    var children = $scope.panelnumber;
                    for (var i = n; i <= children; i++) {
                        var index = $scope.panels.indexOf(i + 1);
                        console.log("animal index" + index);
                        if (index > -1) {
                            $scope.panels.splice(index, 1);
                            localpanelcount -= 1;
                            $scope.panelnumber = localpanelcount;
                            $scope.added[n] = 0;
                            activity_dets = "";
                            $scope.activity_details[i + 1] = [];
                            /*remove remaining animals*/
                            var animeIndex = $scope.remaining_animals.indexOf(i + 1);
                            console.log("animeIndex" + index);
                            if (animeIndex > -1) {
                                $scope.remaining_animals.splice(animeIndex, 1);
                            }
                        }
                    }
                }
            }

            console.log($scope.remaining_animals - parseInt(count));
            if ($scope.remaining_animals[n] - count < 0) {
                toastr.error("number - " + count + " exceeds animals");
                $scope.activity_details[n].animals = "";
                if ($scope.added[n] == 1) {
                    if ($scope.panelnumber > n) {
                        /*delete child panels*/
                        var children = $scope.panelnumber;
                        for (var i = n; i <= children; i++) {
                            var index = $scope.panels.indexOf(i + 1);
                            console.log("animal index" + index);
                            if (index > -1) {
                                $scope.panels.splice(index, 1);
                                localpanelcount -= 1;
                                $scope.panelnumber = localpanelcount;
                                $scope.added[n] = 0;
                                activity_dets = "";
                                $scope.activity_details[i + 1] = [];
                            }
                        }
                    }
                }
                return;
            }
            if ($scope.remaining_animals[n] - count == 0) {
                return;
            }
            if ((count == "") || (count == 0) || (count == undefined)) {
                toastr.error("number cannot be 0 or empty");
                $scope.activity_details[n].animals = "";
                console.log($scope.panels);
                if ($scope.added[n] == 1) {
                    if ($scope.panelnumber > n) {
                        /*delete child panels*/
                        var children = $scope.panelnumber;
                        for (var i = n; i <= children; i++) {
                            var index = $scope.panels.indexOf(i + 1);
                            console.log("empty index" + index);
                            if (index > -1) {
                                $scope.panels.splice(index, 1);
                                localpanelcount -= 1;
                                $scope.panelnumber = localpanelcount;
                                $scope.added[n] = 0;
                                $scope.activity_details[i + 1] = [];
                            }
                        }
                    }
                }
                return;
            } else {
                $scope.added[n] = 1;
                console.log("scope added" + $scope.added[n]);
                $scope.current_count = count;
                $scope.remaining_animals[n + 1] = $scope.remaining_animals[n] - count;
                localpanelcount += 1;
                $scope.panels.push(localpanelcount);
                $scope.panelnumber = localpanelcount;
            }
        }

        $scope.range = function (count) {
            $scope.clicks = count;
            var breeds = [];
            if (count != "") {
                for (var i = 0; i < 1; i++) {
                    breeds.push(i)
                }
            }
            return breeds;
        }

        $scope.addBreed = function () {
            $scope.clicks += 1;
            var activityDiv = angular.element(document.querySelector('#activityDetails'));
            var divv = angular.element(document.querySelector('#orig'));
            var num = $scope.clicks;
            var clonee = divv.clone().prop('id', 'orig' + num);
            var elem = 'orig' + num;
            activityDiv.append(clonee.css('display', 'block'));
            clonee.find('#item-divider').html('#' + num);
        }
        if ($scope.activity_details != undefined) {
            $scope.formatSelect(breeds.length);
        }
        $scope.formatSelect = function (count) {
            console.log(count);
            if (count > 0) {
                for (var j = 0; j < count; j++) {
                    /*breeds list*/
                    $scope.activity_details[j].breed_list = {
                        availableOptions: [
                            {id: '0', name: 'Select a breed'},
                            {id: '1', name: 'freshian'},
                            {id: '2', name: 'breed 2'}
                        ],
                        selectedOption: {id: '0', name: 'Select a Breed'} //This sets the default value of the select in the ui
                    };
                    console.log($scope.activity_details[j].breed_list);
                }
            }
        }
        $scope.onEnd = function (activity_details) {

            console.log(activity_details);
            for (var j = 0; j < $scope.clicks; j++) {

            }

        };
        $scope.status = {
            availableOptions: [
                {id: '1', name: 'Lactating'},
                {id: '2', name: 'Lactating & Serviced'},
                {id: '3', name: 'Serviced'}

            ],
            selectedOption: {id: '0', name: 'Select a Category'} //This sets the default value of the select in the ui
        }

        $scope.activity_details = [];

        $scope.addFarmActivity = function (activity, category, sub_category, details, breed_no) {
            $ionicLoading.show();
            console.log(details);
            console.log(details.length);
            console.log(activity);
            if (activity == undefined || activity.name == undefined || activity.description == undefined || category.id == 0 || sub_category.id == 0 || breed_no == undefined || details.length == 0) {
                toastr.error('Please provide all the activity details');
                $ionicLoading.hide();
                return;
            }
            console.log($scope.panels.length);
            if (details.length - 1 != $scope.panels.length) {
                toastr.error('Please provide all the breed details');
                $ionicLoading.hide();
                return;
            }
            for (var i = 1; i <= $scope.panels.length; i++) {
                if (details[i].breed_list == undefined || details[i].breed_list.selectedOption.id == 0
                    || details[i].date == undefined || details[i].status == undefined || details[i].status.selectedOption.id == 0
                    || details[i].animals == undefined
                ) {
                    toastr.error('Please provide all the breed details for Profile #' + i);
                    $ionicLoading.hide();
                    return;
                }
            }
            activity.farm_activity_categ_id = category.id;
            activity.farm_activity_sub_categ_id = sub_category.id;
            activity.farm_id = $scope.selected_farm_id;
            activity.animals = breed_no;
            $http({
                method: 'POST',
                url: base_url + "api/addFarmActivity",
                data: {
                    activity: activity,
                    user_id: user_id,
                    details: details
                }
            }).success(function (data) {
                $ionicLoading.hide();
                if (data.message == 'success') {
                    $scope.hideFarmActivityModal();
                    $scope.activity = [];
                    $scope.activity_details = [];
                    $scope.getFarmActivitiesfunc();
                    $state.reload();
                    $scope.createActModal();
                    //$state.go('farmActivities', {reload: true});
                } else {
                    console.log(data);
                    toastr.error('Error adding farm activity');
                }
            }).error(function (data) {
                $scope.hideFarmActivityModal();
                $ionicLoading.hide();
            });

        }
        $scope.viewActivityDetails = function (activityDetailsView) {
            console.log(activityDetailsView);
            $scope.activityDetailsView = activityDetailsView;
            $scope.openViewActivityDetailsModal();
        }
        /*create modal to view activity details*/
        //modal to add new farm activity
        $scope.createViewActivityDetailsModal = function () {
            $ionicModal.fromTemplateUrl('templates/farm/viewFarmActivityModal.html', {
                scope: $scope,
                //animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.viewActivityDetailsModal = modal;
            });
        }
        $scope.createViewActivityDetailsModal();
        $scope.openViewActivityDetailsModal = function () {
            $scope.viewActivityDetailsModal.show();
        };
        $scope.hideViewActivityDetailsModal = function () {
            $scope.viewActivityDetailsModal.hide();
            $state.reload();

        }
        $scope.closeViewActivityDetailsModal = function () {
            $scope.viewActivityDetailsModal.remove();
            $state.reload();
        }
        /*        google.maps.event.addDomListener(window, 'load', function () {
         var myLatlng = new google.maps.LatLng(37.3000, -120.4833);

         var mapOptions = {
         center: myLatlng,
         zoom: 16,
         mapTypeId: google.maps.MapTypeId.ROADMAP
         };

         var map = new google.maps.Map(document.getElementById("map"), mapOptions);

         navigator.geolocation.getCurrentPosition(function (pos) {
         map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
         var myLocation = new google.maps.Marker({
         position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
         map: map,
         title: "My Location"
         });
         });

         $scope.map = map;
         });*/
    })
    /*repeat end directive*/
    .directive("repeatEnd", function () {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                if (scope.$last) {
                    scope.$eval(attrs.repeatEnd);
                }
            }
        };
    });