/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

angular.module('starter')
    .controller('notificationCtrl', function ($scope, toastr, $state, $ionicPopup, $http, BASE_URL, $ionicLoading, $ionicPlatform) {


        $ionicLoading.hide();

        var base_url = BASE_URL.base_url;

        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var organisation_id = currentUser.organisation_id;
        var user_id = currentUser.id;

        //get farmers who havent delivered milk today
        $http({
            method: 'POST',
            url: base_url + "api/noMilkToday",
            data: {
                organisation_id: organisation_id
            }
        }).success(function (data) {

            if (data.message == 'success') {
                $scope.totalNumberToday = data.totalNumber;
                $scope.totalFarmersDay = data.farmers;
            } else {
                console.log("error fetching farmers whom we haven't received milk from them");
            }
        }).error(function (data) {

        });

        //get farmers who havent delivered milk this week
        $http({
            method: 'POST',
            url: base_url + "api/noMilkWeek",
            data: {
                organisation_id: organisation_id
            }
        }).success(function (data) {

            if (data.message == 'success') {
                $scope.totalNumberWeek = data.totalNumber;
                $scope.totalFarmersWeek = data.farmers;
            } else {
                console.log("error fetching farmers whom we haven't received milk from them");
            }
        }).error(function (data) {

        });

        //get farmers who havent delivered milk today
        $http({
            method: 'POST',
            url: base_url + "api/noMilkMonth",
            data: {
                organisation_id: organisation_id
            }
        }).success(function (data) {

            if (data.message == 'success') {
                $scope.totalNumberMonth = data.totalNumber;
                $scope.totalFarmersMonth = data.farmers;
            } else {
                console.log("error fetching farmers whom we haven't received milk from them");
            }
        }).error(function (data) {

        });

        $scope.thisDay = function () {
            $state.go('thisDay')
        }

        $scope.thisWeek = function () {
            $state.go('thisWeek')
        }

        $scope.thisMonth = function () {
            $state.go('thisMonth')
        }

    });