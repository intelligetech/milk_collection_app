angular.module('starter')
    .controller('LoginCtrl', function ($scope, $state, $ionicPopup, AuthService, toastr, $ionicLoading) {
        $scope.login = {};

        $scope.login = function (login) {
            $ionicLoading.show();
            if (login == undefined || login.username == undefined || login.password == undefined) {
                toastr.error('Please provide username and password');
                $ionicLoading.hide();
                return;
            }
            AuthService.login(login.username, login.password).then(function (authenticated) {
                $ionicLoading.hide();
                console.log();

                if (AuthService.role_name() == 'Admin') {
                    $state.go('admin_dashboard', {}, {reload: true});
                    $scope.setCurrentUsername(login.username);
                    $scope.setCurrentOrganisationName(AuthService.organisation());
                } else {
                    $state.go('collector_dash', {}, {reload: true});
                    $scope.setCurrentUsername(login.username);
                    $scope.setCurrentOrganisationName(AuthService.organisation());
                }


            }, function (err) {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Login failed!',
                    template: 'Please check your credentials!'
                });
            });
        };
    })