angular.module('starter')
    .controller('dashboardCtrl', function ($scope, $state, $ionicPopup, AuthService, $ionicLoading, $ionicPlatform) {
        //$scope.setCurrentOrganisationName("");
        $ionicPlatform.ready(function () {

        }).then(function () {
            var org = JSON.parse(window.localStorage.getItem('currentOrganisation'));
            if (org != undefined) {
                $scope.organisationName = org.org_name;
            }
        });

        $scope.today = new Date();
        $scope.newFarm = function () {
            console.log("addFarm");
            $ionicLoading.show();
            $state.go('newFarm');
        }
        $scope.profileFarm = function () {
            console.log("profileFarm");
            $ionicLoading.show();
            $state.go('profileFarmList');
        }
        $scope.editFarmer = function () {
            console.log("editFarmer");
        }
        $scope.milkCollection = function () {
            $state.go('milkCollection');
            console.log("milk collection module");
        }
        $scope.addCollection = function () {
            $ionicLoading.show();
            $state.go('addCollection');
        }
        $scope.allCollections = function () {
            $ionicLoading.show();
            $state.go('main.accepted');
        }
        $scope.farmersList = function () {
            $ionicLoading.show();
            $state.go('listFarmers');
        }
        $scope.farmList = function () {
            console.log("farmList");
            $state.go('listFarms');
        }

        $scope.doBack = function () {
            console.log(back);
            $state.go('milkCollection');
        }
        $scope.doBacktodash = function () {
            $state.go('collector_dash');
        }

        $scope.logout = function () {
            AuthService.logout();
            $state.go('login');
        }

        $scope.Cash = function () {
            $ionicLoading.show();
            $state.go('Cash');
        }

        $scope.milkCollected = function () {
            $ionicLoading.show();
            $state.go('Milk');
        }

        $scope.Farm = function () {
            $ionicLoading.show();
            $state.go('Farm');
        }

        $scope.reconciliations = function () {
            $ionicLoading.show();
            $state.go('Reconciliations');
            $ionicLoading.hide();
        }

        $scope.notification = function () {
            $ionicLoading.show();
            $state.go('notification');
        }
    })