angular.module('starter')
    .controller('collectionCtrl', function ($scope, $state, $ionicPopup, AuthService, dataService, $ionicLoading, toastr, $http, BASE_URL) {
        var vm = $scope;
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        $ionicLoading.show();
        $ionicLoading.show();
        $scope.emptyAccepted = false;
        $scope.emptyAccepted = false;
        $scope.tests = [];
        vm.farmer = {};
        vm.farm = {};

        var base_url = BASE_URL.base_url;
        if (currentUser != undefined) {
            $scope.organisation_id = currentUser.organisation_id;
            var organisation_id = currentUser.organisation_id;
            var user_id = currentUser.id;
        }
        $scope.itemArray = [
            {id: 1, name: 'first'},
            {id: 2, name: 'second'},
            {id: 3, name: 'third'},
            {id: 4, name: 'fourth'},
            {id: 5, name: 'fifth'},
        ];

        $scope.selected = {value: $scope.itemArray[0]};
        $scope.doBack = function () {
            if (window.localStorage.getItem('currentRole') == '"Admin"') {
                $state.go("admin_dashboard");
            } else {
                $state.go('milkCollection');
            }

        }
        $scope.doBacktodash = function () {
            $state.go('collector_dash');
        }

        /*do refresh*/
        $scope.doRefresh = function () {
            $scope.loaddata();
        }
        /*get collection list*/

        $scope.loaddata = function () {
            dataService.getCollections(organisation_id);
            $scope.acceptedCollections = dataService.acceptedCollections();
            $scope.rejectedCollections = dataService.rejectedCollections();
            $scope.$broadcast('scroll.refreshComplete');
        }
        $scope.loaddata();
        /*check for undefined*/
        if ($scope.acceptedCollections == undefined) {
            $scope.emptyAccepted = true;
        }
        if ($scope.rejectedCollections == undefined) {
            $scope.emptyRejected = true;
        }
        /*use the data service to obtain the farmers List for the farms*/
        dataService.getFarms(organisation_id);
        $scope.farmList = [];
        /*get farmers with farmers list*/

        $scope.getFarmersFarms = function () {
            $http({
                method: 'POST',
                url: base_url + "api/getFarmersFarms",
                data: {
                    organisation_id: $scope.organisation_id
                }
            }).success(function (data) {
                if (data.message == 'success') {
                    //$scope.routes = data.routes;
                    $scope.all_farmersFarms = data.farmersFarms;
                    $scope.farmersList = data.farmersList;
                    $ionicLoading.hide();
                    /* $scope.farmersSelect = {
                     availableOptions: farmersList,
                     selectedOption: {id: '0', name: 'Select a Route'} //This sets the default value of the select in the ui
                     };*/

                } else {
                    $ionicLoading.hide();
                    console.log("error fetching routes");
                }
            }).error(function (data) {
                $ionicLoading.hide();


            });
        }
        $scope.getFarmersFarms();

        /*get test settings*/
        $scope.getTestSettings = function () {
            $http({
                method: 'POST',
                url: base_url + "api/getTestSettings",
                data: {
                    organisation_id: $scope.organisation_id
                }
            }).success(function (data) {
                if (data.message == 'success') {
                    $scope.testSettings = data.all_tests;
                    $scope.test_names = data.test_names;
                    $ionicLoading.hide();
                } else {
                    $ionicLoading.hide();
                    console.log("error fetching test settings");
                }
            }).error(function (data) {
                $ionicLoading.hide();
                console.log("http error");
            });
        }
        $scope.getTestSettings();
        if ($scope.farmersList == undefined) {
            $ionicLoading.show();
        } else {
            //$ionicLoading.hide();
        }

        /*farms select*/
        var default_option = {id: '0', farm_name: 'Select Farm'}
        $scope.selectedFarm = '';

        $scope.farmsSelect = {
            availableOptions: $scope.farmsList,
            selectedOption: {id: '0', name: 'Select Route'} //This sets the default value of the select in the ui

        }
        if ($scope.farmsList != null) {
            //$ionicLoading.hide();
        }
        /*time select*/
        $scope.time = {
            availableOptions: [
                {id: '0', name: 'Select collection time'},
                {id: '1', name: 'Morning'},
                {id: '2', name: 'Evening'}
            ],
            selectedOption: {id: '0', name: 'Select collection time'} //This sets the default value of the select in the ui
        };
        $scope.changeFarmer = function (farmer) {
            $scope.farmList = $scope.all_farmersFarms[farmer.id]['farms'];
            $scope.farm.selected = undefined ;
            $scope.farmRoute = false;

            /*handle no farms error*/
            if ($scope.farmList.length == 0) {
                toastr.error('The selected Farmer has no farms. Add farms first');
            }
            /* $scope.farmRoute = '';
             $scope.farmList = [];
             var farmer1 = JSON.parse(farmer);
             $scope.farmList = $scope.all_farmersFarms[farmer1.id]['farms'];
             /!*handle no farms error*!/
             if ($scope.farmList.length == 0) {
             toastr.error('The selected Farmer has no farms. Add farms first');
             }*/
        }

        $scope.changeFarm = function (farm) {
            $scope.farmRoute = false;
            //var farmm = JSON.parse(farm);
            $scope.farmRoute = farm.route_description;
        }
        /*Add new milk collection*/
        /*confirm first*/
        $scope.confirmNewCollection = function (collection, farmer_selected, farm_selected, time_selected, tests) {
            $scope.showConfirmNewCollection(collection, farmer_selected, farm_selected, time_selected, tests);
        }
        $scope.newCollection = function (collection, farmer_selected, farm_selected, time_selected, tests) {
            $ionicLoading.show();
            if (farmer_selected == undefined || farm_selected == undefined) {
                toastr.error('Please provide all the collection details');
                $ionicLoading.hide();
                return;
            }
            var farmer = farmer_selected;
            var farm = farm_selected;
            if (collection == undefined || collection.litres_collected == undefined || tests.length == 0 || farmer == undefined || farm == undefined || time_selected.id == 0) {
                toastr.error('Please provide all the collection details');
                $ionicLoading.hide();
                return;
            }
            if (tests.length != $scope.test_names.length) {
                $ionicLoading.hide();
                toastr.error('Please provide all the milk test details');
                return;
            }
            for (var i = 0; i < tests.length; i++) {
                if (tests[i] == null || tests[i] == undefined) {
                    $ionicLoading.hide();
                    toastr.error('Please provide milk test for: ' + $scope.test_names[i]);
                    return;
                }
                if (tests[i][$scope.test_names[i]] == null || tests[i][$scope.test_names[i]] == undefined) {
                    $ionicLoading.hide();
                    toastr.error('Please provide milk test for: ' + $scope.test_names[i]);
                    return;
                }
            }
            collection.organisation_id = organisation_id;
            collection.farm_id = farm.farm_id;
            collection.farmer_id = farmer.id;
            collection.collection_time = time_selected.name;
            collection.tests = tests;
            collection.test_names = $scope.test_names;

            $http({
                method: 'POST',
                url: base_url + "api/addCollection",
                data: {
                    "collection": collection,
                    "user_id": user_id
                }
            }).success(function (data) {
                $ionicLoading.hide();
                if ((data.message == 'success')) {
                    if (data.error_message == undefined) {
                        console.log("success");
                        $scope.showAcceptedConfirm();

                    } else {
                        //toastr.error('Milk failed tests:' + data.error_message);
                        $scope.showFailedConfirm();
                    }
                } else {
                    toastr.error('failed adding collection :' + data.error_message);
                    console.log("error adding collection");
                }
            }).error(function (data) {
                $ionicLoading.hide();
                if (data.message == 'error') {
                    var collection = data.collection;
                    $scope.error_msg = 'The ' + collection.collection_time + ' collection for the farm has already been collected';
                    $scope.showExistingConfirm();

                    // toastr.error('The '+collection.collection_time +' collection for the farm has already been collected');
                }
                console.log("http error");
            });
        };
        // A confirm dialog for failed tests on collection
        $scope.showFailedConfirm = function () {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Failed Tests',
                template: 'The collection failed some tests. Add another collection?'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    $state.reload();
                } else {
                    $state.reload();
                    $state.go('main.accepted', {reload: true});
                }
            });
        };
        // A confirm dialog
        $scope.showAcceptedConfirm = function () {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Collection Accepted',
                template: 'Add another collection?'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    $state.reload();
                } else {
                    $scope.loaddata();
                    $state.go('main.accepted', {reload: true});

                }
            });
        };
        // A confirm dialog for existing collection
        $scope.showExistingConfirm = function () {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Collection already done',
                template: $scope.error_msg + '. Add another collection?'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    $state.reload();
                } else {
                    $state.reload();
                    $state.go('main.accepted', {reload: true});
                }
            });
        };
        // A confirm dialog for existing collection
        $scope.showConfirmNewCollection = function (collection, farmer_selected, farm_selected, time_selected, tests) {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Confirm Add Collection',
                template: 'Add the collection?'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    $scope.newCollection(collection, farmer_selected, farm_selected, time_selected, tests);
                    //$state.reload();
                } else {
                }
            });
        };
        vm.peopleObj = {
            '1': {name: 'Adam', email: 'adam@email.com', age: 12, country: 'United States'},
            '2': {name: 'Amalie', email: 'amalie@email.com', age: 12, country: 'Argentina'},
            '3': {name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina'},
            '4': {name: 'Adrian', email: 'adrian@email.com', age: 21, country: 'Ecuador'},
            '5': {name: 'Wladimir', email: 'wladimir@email.com', age: 30, country: 'Ecuador'},
            '6': {name: 'Samantha', email: 'samantha@email.com', age: 30, country: 'United States'},
            '7': {name: 'Nicole', email: 'nicole@email.com', age: 43, country: 'Colombia'},
            '8': {name: 'Natasha', email: 'natasha@email.com', age: 54, country: 'Ecuador'},
            '9': {name: 'Michael', email: 'michael@email.com', age: 15, country: 'Colombia'},
            '10': {name: 'Nicolás', email: 'nicolas@email.com', age: 43, country: 'Colombia'}
        };

        vm.person = {};

        vm.person.selectedValue = vm.peopleObj[3];
        vm.person.selectedSingle = 'Samantha';
        vm.person.selectedSingleKey = '5';
        // To run the demos with a preselected person object, uncomment the line below.
        //vm.person.selected = vm.person.selectedValue;

        vm.people = [
            {name: 'Adam', email: 'adam@email.com', age: 12, country: 'United States'},
            {name: 'Amalie', email: 'amalie@email.com', age: 12, country: 'Argentina'},
            {name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina'},
            {name: 'Adrian', email: 'adrian@email.com', age: 21, country: 'Ecuador'},
            {name: 'Wladimir', email: 'wladimir@email.com', age: 30, country: 'Ecuador'},
            {name: 'Samantha', email: 'samantha@email.com', age: 30, country: 'United States'},
            {name: 'Nicole', email: 'nicole@email.com', age: 43, country: 'Colombia'},
            {name: 'Natasha', email: 'natasha@email.com', age: 54, country: 'Ecuador'},
            {name: 'Michael', email: 'michael@email.com', age: 15, country: 'Colombia'},
            {name: 'Nicolás', email: 'nicolas@email.com', age: 43, country: 'Colombia'}
        ];

        vm.country = {};
        vm.countries = [ // Taken from https://gist.github.com/unceus/6501985
            {name: 'Afghanistan', code: 'AF'},
            {name: 'Åland Islands', code: 'AX'},
            {name: 'Albania', code: 'AL'},
            {name: 'Algeria', code: 'DZ'},
            {name: 'American Samoa', code: 'AS'},
            {name: 'Andorra', code: 'AD'},
            {name: 'Angola', code: 'AO'},
            {name: 'Anguilla', code: 'AI'},
            {name: 'Antarctica', code: 'AQ'},
            {name: 'Antigua and Barbuda', code: 'AG'},
            {name: 'Argentina', code: 'AR'},
            {name: 'Armenia', code: 'AM'},
            {name: 'Aruba', code: 'AW'},
            {name: 'Australia', code: 'AU'},
            {name: 'Austria', code: 'AT'},
            {name: 'Azerbaijan', code: 'AZ'},
            {name: 'Bahamas', code: 'BS'},
            {name: 'Bahrain', code: 'BH'},
            {name: 'Bangladesh', code: 'BD'},
            {name: 'Barbados', code: 'BB'},
            {name: 'Belarus', code: 'BY'},
            {name: 'Belgium', code: 'BE'},
            {name: 'Belize', code: 'BZ'},
            {name: 'Benin', code: 'BJ'},
            {name: 'Bermuda', code: 'BM'},
            {name: 'Bhutan', code: 'BT'},
            {name: 'Bolivia', code: 'BO'},
            {name: 'Bosnia and Herzegovina', code: 'BA'},
            {name: 'Botswana', code: 'BW'},
            {name: 'Bouvet Island', code: 'BV'},
            {name: 'Brazil', code: 'BR'},
            {name: 'British Indian Ocean Territory', code: 'IO'},
            {name: 'Brunei Darussalam', code: 'BN'},
            {name: 'Bulgaria', code: 'BG'},
            {name: 'Burkina Faso', code: 'BF'},
            {name: 'Burundi', code: 'BI'},
            {name: 'Cambodia', code: 'KH'},
            {name: 'Cameroon', code: 'CM'},
            {name: 'Canada', code: 'CA'},
            {name: 'Cape Verde', code: 'CV'},
            {name: 'Cayman Islands', code: 'KY'},
            {name: 'Central African Republic', code: 'CF'},
            {name: 'Chad', code: 'TD'},
            {name: 'Chile', code: 'CL'},
            {name: 'China', code: 'CN'},
            {name: 'Christmas Island', code: 'CX'},
            {name: 'Cocos (Keeling) Islands', code: 'CC'},
            {name: 'Colombia', code: 'CO'},
            {name: 'Comoros', code: 'KM'},
            {name: 'Congo', code: 'CG'},
            {name: 'Congo, The Democratic Republic of the', code: 'CD'},
            {name: 'Cook Islands', code: 'CK'},
            {name: 'Costa Rica', code: 'CR'},
            {name: 'Cote D\'Ivoire', code: 'CI'},
            {name: 'Croatia', code: 'HR'},
            {name: 'Cuba', code: 'CU'},
            {name: 'Cyprus', code: 'CY'},
            {name: 'Czech Republic', code: 'CZ'},
            {name: 'Denmark', code: 'DK'},
            {name: 'Djibouti', code: 'DJ'},
            {name: 'Dominica', code: 'DM'},
            {name: 'Dominican Republic', code: 'DO'},
            {name: 'Ecuador', code: 'EC'},
            {name: 'Egypt', code: 'EG'},
            {name: 'El Salvador', code: 'SV'},
            {name: 'Equatorial Guinea', code: 'GQ'},
            {name: 'Eritrea', code: 'ER'},
            {name: 'Estonia', code: 'EE'},
            {name: 'Ethiopia', code: 'ET'},
            {name: 'Falkland Islands (Malvinas)', code: 'FK'},
            {name: 'Faroe Islands', code: 'FO'},
            {name: 'Fiji', code: 'FJ'},
            {name: 'Finland', code: 'FI'},
            {name: 'France', code: 'FR'},
            {name: 'French Guiana', code: 'GF'},
            {name: 'French Polynesia', code: 'PF'},
            {name: 'French Southern Territories', code: 'TF'},
            {name: 'Gabon', code: 'GA'},
            {name: 'Gambia', code: 'GM'},
            {name: 'Georgia', code: 'GE'},
            {name: 'Germany', code: 'DE'},
            {name: 'Ghana', code: 'GH'},
            {name: 'Gibraltar', code: 'GI'},
            {name: 'Greece', code: 'GR'},
            {name: 'Greenland', code: 'GL'},
            {name: 'Grenada', code: 'GD'},
            {name: 'Guadeloupe', code: 'GP'},
            {name: 'Guam', code: 'GU'},
            {name: 'Guatemala', code: 'GT'},
            {name: 'Guernsey', code: 'GG'},
            {name: 'Guinea', code: 'GN'},
            {name: 'Guinea-Bissau', code: 'GW'},
            {name: 'Guyana', code: 'GY'},
            {name: 'Haiti', code: 'HT'},
            {name: 'Heard Island and Mcdonald Islands', code: 'HM'},
            {name: 'Holy See (Vatican City State)', code: 'VA'},
            {name: 'Honduras', code: 'HN'},
            {name: 'Hong Kong', code: 'HK'},
            {name: 'Hungary', code: 'HU'},
            {name: 'Iceland', code: 'IS'},
            {name: 'India', code: 'IN'},
            {name: 'Indonesia', code: 'ID'},
            {name: 'Iran, Islamic Republic Of', code: 'IR'},
            {name: 'Iraq', code: 'IQ'},
            {name: 'Ireland', code: 'IE'},
            {name: 'Isle of Man', code: 'IM'},
            {name: 'Israel', code: 'IL'},
            {name: 'Italy', code: 'IT'},
            {name: 'Jamaica', code: 'JM'},
            {name: 'Japan', code: 'JP'},
            {name: 'Jersey', code: 'JE'},
            {name: 'Jordan', code: 'JO'},
            {name: 'Kazakhstan', code: 'KZ'},
            {name: 'Kenya', code: 'KE'},
            {name: 'Kiribati', code: 'KI'},
            {name: 'Korea, Democratic People\'s Republic of', code: 'KP'},
            {name: 'Korea, Republic of', code: 'KR'},
            {name: 'Kuwait', code: 'KW'},
            {name: 'Kyrgyzstan', code: 'KG'},
            {name: 'Lao People\'s Democratic Republic', code: 'LA'},
            {name: 'Latvia', code: 'LV'},
            {name: 'Lebanon', code: 'LB'},
            {name: 'Lesotho', code: 'LS'},
            {name: 'Liberia', code: 'LR'},
            {name: 'Libyan Arab Jamahiriya', code: 'LY'},
            {name: 'Liechtenstein', code: 'LI'},
            {name: 'Lithuania', code: 'LT'},
            {name: 'Luxembourg', code: 'LU'},
            {name: 'Macao', code: 'MO'},
            {name: 'Macedonia, The Former Yugoslav Republic of', code: 'MK'},
            {name: 'Madagascar', code: 'MG'},
            {name: 'Malawi', code: 'MW'},
            {name: 'Malaysia', code: 'MY'},
            {name: 'Maldives', code: 'MV'},
            {name: 'Mali', code: 'ML'},
            {name: 'Malta', code: 'MT'},
            {name: 'Marshall Islands', code: 'MH'},
            {name: 'Martinique', code: 'MQ'},
            {name: 'Mauritania', code: 'MR'},
            {name: 'Mauritius', code: 'MU'},
            {name: 'Mayotte', code: 'YT'},
            {name: 'Mexico', code: 'MX'},
            {name: 'Micronesia, Federated States of', code: 'FM'},
            {name: 'Moldova, Republic of', code: 'MD'},
            {name: 'Monaco', code: 'MC'},
            {name: 'Mongolia', code: 'MN'},
            {name: 'Montserrat', code: 'MS'},
            {name: 'Morocco', code: 'MA'},
            {name: 'Mozambique', code: 'MZ'},
            {name: 'Myanmar', code: 'MM'},
            {name: 'Namibia', code: 'NA'},
            {name: 'Nauru', code: 'NR'},
            {name: 'Nepal', code: 'NP'},
            {name: 'Netherlands', code: 'NL'},
            {name: 'Netherlands Antilles', code: 'AN'},
            {name: 'New Caledonia', code: 'NC'},
            {name: 'New Zealand', code: 'NZ'},
            {name: 'Nicaragua', code: 'NI'},
            {name: 'Niger', code: 'NE'},
            {name: 'Nigeria', code: 'NG'},
            {name: 'Niue', code: 'NU'},
            {name: 'Norfolk Island', code: 'NF'},
            {name: 'Northern Mariana Islands', code: 'MP'},
            {name: 'Norway', code: 'NO'},
            {name: 'Oman', code: 'OM'},
            {name: 'Pakistan', code: 'PK'},
            {name: 'Palau', code: 'PW'},
            {name: 'Palestinian Territory, Occupied', code: 'PS'},
            {name: 'Panama', code: 'PA'},
            {name: 'Papua New Guinea', code: 'PG'},
            {name: 'Paraguay', code: 'PY'},
            {name: 'Peru', code: 'PE'},
            {name: 'Philippines', code: 'PH'},
            {name: 'Pitcairn', code: 'PN'},
            {name: 'Poland', code: 'PL'},
            {name: 'Portugal', code: 'PT'},
            {name: 'Puerto Rico', code: 'PR'},
            {name: 'Qatar', code: 'QA'},
            {name: 'Reunion', code: 'RE'},
            {name: 'Romania', code: 'RO'},
            {name: 'Russian Federation', code: 'RU'},
            {name: 'Rwanda', code: 'RW'},
            {name: 'Saint Helena', code: 'SH'},
            {name: 'Saint Kitts and Nevis', code: 'KN'},
            {name: 'Saint Lucia', code: 'LC'},
            {name: 'Saint Pierre and Miquelon', code: 'PM'},
            {name: 'Saint Vincent and the Grenadines', code: 'VC'},
            {name: 'Samoa', code: 'WS'},
            {name: 'San Marino', code: 'SM'},
            {name: 'Sao Tome and Principe', code: 'ST'},
            {name: 'Saudi Arabia', code: 'SA'},
            {name: 'Senegal', code: 'SN'},
            {name: 'Serbia and Montenegro', code: 'CS'},
            {name: 'Seychelles', code: 'SC'},
            {name: 'Sierra Leone', code: 'SL'},
            {name: 'Singapore', code: 'SG'},
            {name: 'Slovakia', code: 'SK'},
            {name: 'Slovenia', code: 'SI'},
            {name: 'Solomon Islands', code: 'SB'},
            {name: 'Somalia', code: 'SO'},
            {name: 'South Africa', code: 'ZA'},
            {name: 'South Georgia and the South Sandwich Islands', code: 'GS'},
            {name: 'Spain', code: 'ES'},
            {name: 'Sri Lanka', code: 'LK'},
            {name: 'Sudan', code: 'SD'},
            {name: 'Suriname', code: 'SR'},
            {name: 'Svalbard and Jan Mayen', code: 'SJ'},
            {name: 'Swaziland', code: 'SZ'},
            {name: 'Sweden', code: 'SE'},
            {name: 'Switzerland', code: 'CH'},
            {name: 'Syrian Arab Republic', code: 'SY'},
            {name: 'Taiwan, Province of China', code: 'TW'},
            {name: 'Tajikistan', code: 'TJ'},
            {name: 'Tanzania, United Republic of', code: 'TZ'},
            {name: 'Thailand', code: 'TH'},
            {name: 'Timor-Leste', code: 'TL'},
            {name: 'Togo', code: 'TG'},
            {name: 'Tokelau', code: 'TK'},
            {name: 'Tonga', code: 'TO'},
            {name: 'Trinidad and Tobago', code: 'TT'},
            {name: 'Tunisia', code: 'TN'},
            {name: 'Turkey', code: 'TR'},
            {name: 'Turkmenistan', code: 'TM'},
            {name: 'Turks and Caicos Islands', code: 'TC'},
            {name: 'Tuvalu', code: 'TV'},
            {name: 'Uganda', code: 'UG'},
            {name: 'Ukraine', code: 'UA'},
            {name: 'United Arab Emirates', code: 'AE'},
            {name: 'United Kingdom', code: 'GB'},
            {name: 'United States', code: 'US'},
            {name: 'United States Minor Outlying Islands', code: 'UM'},
            {name: 'Uruguay', code: 'UY'},
            {name: 'Uzbekistan', code: 'UZ'},
            {name: 'Vanuatu', code: 'VU'},
            {name: 'Venezuela', code: 'VE'},
            {name: 'Vietnam', code: 'VN'},
            {name: 'Virgin Islands, British', code: 'VG'},
            {name: 'Virgin Islands, U.S.', code: 'VI'},
            {name: 'Wallis and Futuna', code: 'WF'},
            {name: 'Western Sahara', code: 'EH'},
            {name: 'Yemen', code: 'YE'},
            {name: 'Zambia', code: 'ZM'},
            {name: 'Zimbabwe', code: 'ZW'}
        ];
    })