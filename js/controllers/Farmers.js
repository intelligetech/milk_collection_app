angular.module('starter')
    .controller('Farmers', function ($scope, $state, $ionicPopup, $ionicLoading, toastr, $ionicModal, $http, BASE_URL, FarmService, dataService) {
        var base_url = BASE_URL.base_url;
        $http({
            method: 'POST',
            url: base_url + 'api/farmerCash',
            data: {
                user_id: 1
            }
        }).success(function (data) {
            if (data.message == 'success') {
               $scope.cash = data.cash;
                $ionicLoading.hide();
                $state.go('Cash');
            } else {
                console.log("error fetching farmer's cash record");
            }
        }).error(function (data) {

        });



    });